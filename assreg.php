<?php
    session_start();
    if(isset($_POST['submit']))
    {
       $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "php_assess";

        // Create connection
        $conn = mysqli_connect($servername, $username, $password,$dbname);

        // Check connection
        if ($conn->connect_error)
        {
            die("Connection failed: " .$conn->connect_error);
        }

        $sql = "SELECT * FROM form_details where name = '".$_POST['name']."' and email = '".$_POST['email']."'";
        $result = $conn->query($sql);
        
        if (!($result->num_rows > 0)) 
        {
            $name = $_POST['name']; 
            $user_password = $_POST['password'];
            $confirm_password = $_POST['confirm_password'];
            $age=$_POST['age'];
            $email = $_POST['email'];  
            $option_select=$_POST['option'];
            $phone_number = $_POST['phone'];
            $zip_code=$_POST['zip_code'];
            $money=$_POST['money'];
            $birthday=$_POST['birthday'];
            $web_site=$_POST['website'];
            $comment=$_POST['comment'];
            $err = 0;
        
            // mandatory fields validation 

            if (empty($_POST["name"])) 
            {
                $err_arr['name'] = "Name is required";
                $err=1;
            }

            if (empty($_POST["password"])) 
            {
                $err_arr['password'] = "password is required";
                $err=1;
            }

            if (empty($_POST["confirm_password"])) 
            {
                $err_arr['confirm_password'] = "confirm password is required";
                $err=1;
            }
            
            if (empty($_POST["age"])) 
            {
                $err_arr['age'] = "age is required";
                $err=1;
            }

            if (empty($_POST["birthday"])) 
            {
                $err_arr['birthday'] = "Birthday is required";
                $err=1;
            }

            if (empty($_POST["option"])) 
            {
                $err_arr['option'] = "option is required";
                $err=1;
            }


            if (empty($_POST["money"])) 
            {
                $err_arr['money'] = "Money number is required";
                $err=1;
            
            }

            if (empty($_POST["email"])) 
            {
                $err_arr['email'] = "Email is required"; 
                $err=1;
                
            }
            //email validation

            else if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email))
            {
                $err_arr['email'] =  "Not a valid email";       
                $err = 1;
            }

            if (empty($_POST["phone"])) 
            {
                $err_arr['phone'] = "Phone number is required";   
                $err=1;
            }

            //phone number validation

           else if(!preg_match("/^[0-9]{10}$/",$phone_number)) 
            {
                $err_arr['phone']="Not valid phone number";
                $err=1;
            } 


            if (empty($_POST["zip_code"])) 
            {
                $err_arr['zip_code']= "zip code is required";
                $err=1;
            }

            // zip code validation

           else  if(!preg_match('/^\d{6}$/', $zip_code)) 
            {
                $err_arr['zip_code']= "Not a valid zip code";
                $err=1;
            }
            
            if (empty($_POST["website"])) 
            {
                $err_arr['website']= "url is required";
                $err=1;
            }
            
            // url validation
            else if(preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$web_site))
            {
                $err_arr['web_site']= "Not a valid web site url";
                $err=1;
            }
            
            
            if($err==0)
            {
                $sql = "INSERT INTO form_details (name,password,confirm_password,age,email,option_select,phone_number,zip_code,money,birthday,website,comment)
                VALUES ('".$name."', '".$user_password."', '".$confirm_password."', '".$age."', '".$email."', '".$option_select."', '".$phone_number."','".$zip_code."','".$money."','".$birthday."','".$web_site."','".$comment."')";
                $result = $conn->query($sql);
                $_SESSION['last_id'] = mysqli_insert_id($conn);
                $conn->close();

                if($result)
                {
                    print_r($_SESSION);
                    header("location:user_page.php");
                }
            }

            else
            {
                $_SESSION['error_msgs'] = $err_arr;
                $_SESSION['old_data'] = $_POST;
            }
            
        }
        else
        {
            $err_arr['name']="Name already exists";
            $err_arr['email'] = "Email already exists";
            $_SESSION['error_msgs'] = $err_arr;
        }
        
    }
?>


<html>
    <head> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <link href="bootstrap-datetimepicker-master/bootstrap-datetimepicker-master/sample in bootstrap v3/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap-datetimepicker-master/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="assreg.css">
    </head>
    
	<body>
		<div class="container" id="form">

			<div class="row main">

				<div class="main-login main-center">

                    <p><center><b>REGISTER FORM<b></center></p>
                    
                    <form enctype="multipart/form-data" method="post" action="assreg.php">
                         
                         <!-- Name field starts -->

						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Name*</label>
							<div class="cols-sm-10">

								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name" value="<?php if(isset($_SESSION['old_data']['name'])){ echo $_SESSION['old_data']['name']; } ?>" placeholder="Enter your Name"/>
                                </div>
                                
                                <div class="col-md-6">
                                    <?php
                                        if(isset($_SESSION['error_msgs']['name']) && !empty($_SESSION['error_msgs']['name'])) 
                                        {
                                            echo"<div style='color:red'>" .$_SESSION['error_msgs']['name']. "</div>";
                                        }
                                    ?>
                                </div>
							</div>
                        </div>
                        <!-- Name field ends -->

                        <!--Password starts  -->

                        <div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password*</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password" value="<?php if(isset($_SESSION['old_data']['password'])){ echo $_SESSION['old_data']['password']; } ?>"placeholder="Enter your Password"/>
                                </div>
                                <div class="col-md-8">
                                    <?php
                                        if(isset($_SESSION['error_msgs']['password']) && !empty($_SESSION['error_msgs']['password'])) 
                                        {
                                            echo"<div style='color:red'>" .$_SESSION['error_msgs']['password']. "</div>";
                                        }
                                    ?>
                                </div>
							</div>
                        </div>
                        
                         <!--Password ends  -->
                         
                          <!--confirm Password starts  -->

                        <div class="form-group">
							<label for="confirm password" class="cols-sm-2 control-label">confirm Password*</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirm_password" id="confirm_password" value="<?php if(isset($_SESSION['old_data']['confirm_password'])){ echo $_SESSION['old_data']['confirm_password']; } ?>" placeholder="Confirm Password"/>
                                </div>
                                <div class="col-md-12">
                                    <?php
                                        if(isset($_SESSION['error_msgs']['confirm_password']) && !empty($_SESSION['error_msgs']['confirm_password'])) 
                                        {
                                            echo"<div style='color:red'>" .$_SESSION['error_msgs']['confirm_password']. "</div>";
                                        }
                                    ?>
                                </div>
							</div>
                        </div>
                        
                        <!--confirm Password starts  -->
                         
                          <!--Age starts  -->

                        <div class="form-group">
							<label for="age" class="cols-sm-2 control-label">Age</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-heart" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="age" id="age" readonly value="<?php if(isset($_SESSION['old_data']['age'])){ echo $_SESSION['old_data']['age']; } ?>"  placeholder="Enter your age "/>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                            if(isset($_SESSION['error_msgs']['age']) && !empty($_SESSION['error_msgs']['age'])) 
                                            {
                                                echo"<div style='color:red'>" .$_SESSION['error_msgs']['age']. "</div>";
                                            }
                                        ?>
                                </div>
							</div>
                        </div>

                         <!--Age ends -->

                        <!-- Email starts -->

                        <div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Email*</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email" value="<?php if(isset($_SESSION['old_data']['email'])){ echo $_SESSION['old_data']['email']; } ?>"  placeholder="Enter your Email"/>
                                </div>
                                <div class="col-md-6">
                                    <?php
                                            if(isset($_SESSION['error_msgs']['email']) && !empty($_SESSION['error_msgs']['email'])) 
                                            {
                                                echo"<div style='color:red'>" .$_SESSION['error_msgs']['email']. "</div>";
                                            }
                                    ?>
                                </div>
							</div>
                        </div>
                        <!-- Email ends -->
                        
                        <!-- option starts -->

                        <div class="form-group">
                            <label for="option" class="cols-sm-2 control-label">Option</label>
                            <div class="cols-sm-10">
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-list-ol" aria-hidden="true"></i></span> 
                                    <select class="form-control" name="option" value="<?php if(isset($_SESSION['old_data']['option'])){ echo $_SESSION['old_data']['option']; } ?>"id="option">
                                        <option value="">please select your option</option>
                                        <option>student</option>
                                        <option>Bussiness</option>
                                        <option>Member</option>
                                    </select>
                                </div>    
                                <div class="col-md-6">
                                    <?php
                                            if(isset($_SESSION['error_msgs']['option']) && !empty($_SESSION['error_msgs']['option'])) 
                                            {
                                                echo"<div style='color:red'>" .$_SESSION['error_msgs']['option']. "</div>";
                                            }
                                    ?>
                                </div>
                            </div>
                        </div>    
                        
                        <!-- option ends -->

                        <!-- phone number starts -->

                        <div class="form-group">
							<label for="phone number" class="cols-sm-2 control-label">Phone Number*</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-mobile"  aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="phone" id="phone_no" value="<?php if(isset($_SESSION['old_data']['phone'])){ echo $_SESSION['old_data']['phone']; } ?>" placeholder="Enter your phone number"/>
                                </div>
                                <div class="col-md-12">
                                    <?php
                                            if(isset($_SESSION['error_msgs']['phone']) && !empty($_SESSION['error_msgs']['phone'])) 
                                            {
                                                echo"<div style='color:red'>" .$_SESSION['error_msgs']['phone']. "</div>";
                                            }
                                    ?>
                                </div>
							</div>
						</div>
                        
                        <!--phone number ends -->
                        
                        <!--zip code starts -->

                        <div class="form-group">
							<label for="zip code" class="cols-sm-2 control-label">Zip Code*</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-list-ol" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="zip_code" id="zip_code" value="<?php if(isset($_SESSION['old_data']['zip_code'])){ echo $_SESSION['old_data']['zip_code']; } ?>" placeholder="Enter your zip code number"/>
                                </div>
                                <div class="col-md-8">
                                    <?php
                                       if(isset($_SESSION['error_msgs']['zip_code']) && !empty($_SESSION['error_msgs']['zip_code'])) 
                                       {
                                           echo"<div style='color:red'>" .$_SESSION['error_msgs']['zip_code']. "</div>";
                                       }
                                    ?>
                                </div>
							</div>
						</div>

                        <!--zip code ends -->
                        
                          <!--money starts -->

                         <div class="form-group">
							<label for="money" class="cols-sm-2 control-label">Money</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="money" id="money" value="<?php if(isset($_SESSION['old_data']['money'])){ echo $_SESSION['old_data']['money']; } ?>" placeholder="Enter your money number"/>
                                </div>
                                <div class="col-md-12">
                                    <?php
                                       if(isset($_SESSION['error_msgs']['money']) && !empty($_SESSION['error_msgs']['money'])) 
                                       {
                                           echo"<div style='color:red'>" .$_SESSION['error_msgs']['money']. "</div>";
                                       }
                                    ?>
                                </div>
							</div>
                        </div>
                        
                        <!--money ends -->

                        <!--birthday starts -->

                        <div class="form-group">
                            <label for="dtp_input2" class="col-sm-2 control-label">Birthday*</label><br>
                            <div class="cols-sm-10">
                                <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd"  style="width:100%;">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar" style="color:brown;"></span></span> 
                                    <input class="form-control" type="text" name="birthday" id="dob" value="<?php if(isset($_SESSION['old_data']['birthday'])){ echo $_SESSION['old_data']['birthday']; } ?>"  placeholder="yyyy-mm-dd">
                                </div>
                                <div class="col-md-8">
                                    <?php
                                        if(isset($_SESSION['error_msgs']['birthday']) && !empty($_SESSION['error_msgs']['birthday'])) 
                                        {
                                            echo"<div style='color:red'>" .$_SESSION['error_msgs']['birthday']. "</div>";
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        
                        <!--birthday ends -->
                        
                        <!--website starts -->

                        <div class="form-group">
							<label for="website" class="cols-sm-2 control-label">Website</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="website" id="website" value="<?php if(isset($_SESSION['old_data']['website'])){ echo $_SESSION['old_data']['website']; } ?>" placeholder="http://google.com"/>
                                </div>
                                <div class="col-md-8">
                                    <?php
                                       if(isset($_SESSION['error_msgs']['website']) && !empty($_SESSION['error_msgs']['website'])) 
                                       {
                                           echo"<div style='color:red'>" .$_SESSION['error_msgs']['website']. "</div>";
                                       }
                                    ?>
                                </div>
							</div>
                        </div>
                        
                        <!--website ends-->
                        
                        <!--comment starts -->

                        <div class="form-group">
                            <label for="comment" class="cols-sm-2 control-label">Comment</label>
                            <div class="cols-sm-10">
								<div class="input-group">
                                   <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                   <textarea class="form-control" rows="1" name="comment" id="comment" value="<?php if(isset($_SESSION['old_data']['comment'])){ echo $_SESSION['old_data']['comment']; } ?>"></textarea>
                                </div>   
                                <div class="col-md-8">
                                </div>
                            </div>
                        </div>

                        <!--comment ends -->
                        
                        <!--button starts -->

                        <div class="form-group ">
							<button type="submit" id="button" name="submit" value="submit" class="btn btn-primary btn-lg btn-block login-button">Sign up</button>
						</div>
                        
                        <!--button ends -->

                    </form>    
                </div>    
            </div>    
        </div>   
            
            <!-- datepicker code starts -->

            <script type="text/javascript" src="bootstrap-datetimepicker-master/bootstrap-datetimepicker-master/sample in bootstrap v3/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
            <script type="text/javascript" src="bootstrap-datetimepicker-master/bootstrap-datetimepicker-master/sample in bootstrap v3/bootstrap/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="bootstrap-datetimepicker-master/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
            <script type="text/javascript" src="bootstrap-datetimepicker-master/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.uk.js" charset="UTF-8"></script>
            <script type="text/javascript">
                $('.form_date').datetimepicker({
                    language:  'fr',
                    weekStart: 1,
                    todayBtn:  1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0
                });
                
                // datepicker ends

                //age and dob populate code
                
                $(document).ready(function()
                {
                    $("#dob").change(function()
                    {
                        var dob1 =$(this).val(); 
                        var dob = new Date(dob1);
                        var today = new Date();
                        var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                        $('#age').val(age);
                        $('#age').html(age+' years old');
                    });
            });

                
        </script>

    </body>     
</html>    

<?php
  session_destroy();
?>