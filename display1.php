<?php
if(isset($_GET['id'])&& !empty($_GET['id']))
{
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "php_assess";

        //create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        //check connection
        if ($conn->connect_error) 
        {
            die("Connection failed: " . $conn->connect_error);
        } 

        $sql="select * from form_details where id='".$_GET["id"]."'";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();  
        $conn->close();
        
                              

?>


<html>
    <head> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="assreg.css">
    </head>
    
	<body>
		<div class="container">
			<div class="row main">
				<div class="main-login main-center">

                    <p><center>EDIT FORM</center></p>

					<form enctype="multipart/form-data" method="post" action="update1.php">

						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Your Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name" 
                                    value="<?php if(isset($row["name"]))
                                    {
                                        echo $row["name"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };

                                    ?>">
								</div>
							</div>
						</div>

                        <div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password*</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password" placeholder="Enter your Password"
                                    value="<?php if(isset($row["password"]))
                                    {
                                        echo $row["password"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };

                                    ?>">
                                </div>
							</div>
                        </div>

                         <div class="form-group">
							<label for="confirm password" class="cols-sm-2 control-label">confirm Password*</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirm_password" id="confirm_password"  placeholder="Confirm Password"
                                    value="<?php if(isset($row["confirm_password"]))
                                    {
                                        echo $row["confirm_password"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };
                                    ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
							<label for="age" class="cols-sm-2 control-label">Age</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-heart" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="age" id="age" placeholder="Enter your age "
                                    value="<?php if(isset($row["age"]))
                                    {
                                        echo $row["age"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };
                                    ?>">
                                </div>
							</div>
                        </div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email" 
                                    value="<?php if(isset($row["email"]))
                                    {
                                        echo $row["email"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };

                                    ?>">
                                </div>
                                
							</div>
						</div>
                        
                        
                        <div class="form-group">
							<label for="phone number" class="cols-sm-2 control-label">Phone Number</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-mobile"  aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="phone" id="phone_no"  placeholder="Enter your phone number"
                                    value="<?php if(isset($row["phone_number"]))
                                    {
                                        echo $row["phone_number"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };

                                    ?>">
                                </div>
                               
							</div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label for="option" class="cols-sm-2 control-label">Option</label>
                            <div class="cols-sm-10">
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-list-ol" aria-hidden="true"></i></span> 
                                    <select class="form-control" name="option" id="option">
                                     
                                        <option >please select your option</option>
                                        <option  value="Student" <?php if ($row['option_select'] == "Student"){ echo 'selected'; } else{ echo '';}?> >student</option>
                                        <option value="Bussiness" <?php if ($row['option_select'] == "Bussiness"){ echo 'selected'; } else{ echo '';}?> > Bussiness</option>
                                        <option value="Member" <?php if ($row['option_select'] == "Member"){ echo 'selected'; } else{ echo '';}?> >Member</option>
                                    
                                    </select>
                                </div>    
                                
                            </div>
                        </div>    
                        
                        <div class="form-group">
							<label for="zip code" class="cols-sm-2 control-label">Zip Code*</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-list-ol" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="zip_code" id="zip_code" placeholder="Enter your zip code number"
                                    value="<?php if(isset($row["zip_code"]))
                                    {
                                        echo $row["zip_code"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };

                                    ?>">
                                </div>
							</div>
						</div>

                         <div class="form-group">
							<label for="money" class="cols-sm-2 control-label">Money</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="money" id="money"  placeholder="Enter your money number"
                                    value="<?php if(isset($row["money"]))
                                    {
                                        echo $row["money"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };

                                    ?>">
                                </div>
							</div>
                        </div>
                        
                        <div class="form-group">
                            <label for="dtp_input2" class="col-sm-2 control-label">Birthday*</label><br>
                            <div class="cols-sm-10">
                                <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd"  style="width:100%;">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar" style="color:brown;"></span></span> 
                                    <input class="form-control" type="text" name="birthday" id="dob"  placeholder="yyyy-mm-dd"
                                    value="<?php if(isset($row["birthday"]))
                                    {
                                        echo $row["birthday"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };
                                    ?>">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
							<label for="website" class="cols-sm-2 control-label">Website</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="website" id="website" placeholder="http://google.com"
                                        value="<?php if(isset($row["website"]))
                                    {
                                        echo $row["website"];
                                    }
                                    else
                                    {
                                    echo '';
                                    };
                                    ?>">
                                </div>
							</div>
                        </div>

						<div class="form-group ">
                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>" class="btn">
							<button type="submit" id="button" name="submit" value="submit" class="btn btn-primary btn-lg btn-block login-button">Submit</button>
							
					    </div>
						
					</form>
				</div>
			</div>
        </div>
    </body>
</html>


<?php
}
 else
 {
   echo "id should not be null";
 }

?>